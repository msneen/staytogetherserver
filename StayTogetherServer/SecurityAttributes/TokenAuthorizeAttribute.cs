﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace StayTogetherServer.SecurityAttributes
{
    public class TokenAuthorizeAttribute: AuthorizeAttribute
    {
        public override bool AuthorizeHubConnection(HubDescriptor hubDescriptor, IRequest request)
        {
            if (request.Headers["AuthToken"] == "x0y2!tJyHR%$Sip@*%amaGxvs")
            {
                return true; //return base.AuthorizeHubConnection(hubDescriptor, request);
            };
            return false;
        }

        public override bool AuthorizeHubMethodInvocation(IHubIncomingInvokerContext hubIncomingInvokerContext, bool appliesToMethod)
        {
            if (hubIncomingInvokerContext.Hub.Context.Request.Headers["AuthToken"] == "x0y2!tJyHR%$Sip@*%amaGxvs")
            {
                return true; //return base.AuthorizeHubConnection(hubDescriptor, request);
            };
            return false;
            //return base.AuthorizeHubMethodInvocation(hubIncomingInvokerContext, appliesToMethod);
        }
    }
}