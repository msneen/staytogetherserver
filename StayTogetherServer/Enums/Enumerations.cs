﻿namespace StayTogetherServer.Enums
{
    public enum DistanceCalculationType
    {
        DistanceFromLeader,
        CenterOfGravity,
        DaisyChain
    }
}