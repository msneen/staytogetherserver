﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights;
using Microsoft.AspNet.SignalR;
using StayTogetherServer.DistanceCalculator;
using StayTogetherServer.Enums;
using StayTogetherServer.Extensions;
using StayTogetherServer.Models;
using StayTogetherServer.SecurityAttributes;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

[assembly: InternalsVisibleTo("StayTogetherServer.Test")]
namespace StayTogetherServer
{
    /// <summary>
    /// Provides communication between group members
    /// Messages to Client:
    ///     groupDisbanded(group.Key)
    ///     MemberLeft(memberAlreadyInGroup.PhoneNumber, memberAlreadyInGroup.Name);
    ///     updateGroupId(group.Key)
    ///     groupInvitation(phoneNumerOfLeader, nameOfLeader)
    ///     someoneIsLost(LostMemberVm)   //member.PhoneNumber, member.Latitude, member.Longitude, member.Name, distanceFromGroupLeader
    ///     MemberAlreadyInGroup(member.PhoneNumber, member.Name);
    ///     groupPositionUpdate(group.Value.GroupMembers)
    ///     requestMemberLocations(group.Value.PhoneNumber);
    ///     broadcastMessage(name, message)
    ///     groupMessage(member, message)
    /// </summary>
    [TokenAuthorize]
	public class StayTogetherHub : Hub
	{
        private static readonly TelemetryClient _telemetry = new TelemetryClient();
		// The key is the group name (the group leader's phone number); the list is the group members.
		private static readonly ConcurrentDictionary<string, GroupVm> StayTogetherGroups = new ConcurrentDictionary<string, GroupVm>();
        private static readonly ConcurrentDictionary<string, GroupMemberVm> MemberIndex = new ConcurrentDictionary<string, GroupMemberVm>();
	    private static string _accountSid;
	    private static string _authToken;
	    private static string _fromPhoneNumber;

	    private static DateTime _lastGroupCleanup = DateTime.Now;
        private static DateTime _lastMemberIndexCleanup = DateTime.Now;

	     ~StayTogetherHub()
	    {
	        if(DateTime.Now.ToUniversalTime().Subtract(_lastGroupCleanup) > TimeSpan.FromMinutes(30))
	        {
	            _lastGroupCleanup = DateTime.Now;
                RemoveInactiveGroups();
	        }

	        if(DateTime.Now.ToUniversalTime().Subtract(_lastMemberIndexCleanup) > TimeSpan.FromHours(3))
	        {
	            _lastMemberIndexCleanup = DateTime.Now;
	            RemoveInactiveMembers();
	        }
	    }

	    #region Public SignalR Methods
        /// <summary>
		/// The group leader calls this function from the Xamarin application. 
		/// A SignalR group is created by adding a member to it - not in this method.
		/// 
		/// The group can be used to filter clients who receive broadcasts.
		/// The group name will consist of the group leader's phone number (consistent format?)
		/// Track the groups in a separate collection.
		/// </summary>
		public void CreateGroup(GroupVm groupVm)
		{
			StayTogetherGroups.RemoveGroup(groupVm);//RemoveGroup(groupVm);

            groupVm.GroupMembers.RemoveDuplicates();   //RemoveDuplicates(groupVm);

            //update times
		    var groupTimeout = groupVm.GroupDisbandDateTime.Subtract(groupVm.GroupCreatedDateTime);
            groupVm.GroupCreatedDateTime = DateTime.Now.ToUniversalTime();
            groupVm.GroupDisbandDateTime = DateTime.Now.Add(groupTimeout).ToUniversalTime();

            Groups.Add(Context.ConnectionId, groupVm.PhoneNumber);

            SendGroupInviteToNewMembers(groupVm);

			var groupAdded = StayTogetherGroups.TryAdd(groupVm.PhoneNumber, groupVm); 
			Contract.Assert(groupAdded, string.Format(@"Couldn't create the group [{0}]", groupVm.PhoneNumber));
		}

	    public void AddToGroup(GroupVm groupVm)
	    {
            AddToGroup(groupVm, false);
	    }

	    public void AddToGroup(GroupVm groupVm, bool force)
        {
            _telemetry.TrackEvent("AddToGroup_Start");        
            var groupLeader = groupVm.GroupMembers[0];
            var group = string.IsNullOrEmpty(groupLeader.GroupId) ? FindGroupByMember(groupLeader) : FindGroupByGroupId(groupLeader);

            if(!@group.IsValid()) return;
            _telemetry.TrackEvent($"AddToGroup_Group_{group.Value.PhoneNumber}_Found");

            SendGroupInviteToNewMembers(groupVm);

            for(var i = 1; i < groupVm.GroupMembers.Count; i++)
            {
                var member = groupVm.GroupMembers[i];
                _telemetry.TrackEvent($"AddToGroup_AddingGroupMember{member.PhoneNumber}");
                var added = @group.Value.GroupMembers.AddIfNotIncluded(groupVm.GroupMembers[i]);

                _telemetry.TrackEvent(added
                    ? $"AddToGroup_AddedGroupMember{member.PhoneNumber}"
                    : $"AddToGroup_GroupMember{member.PhoneNumber}_NotAddedBecauseAlreadyInGroup");
            }
        }

        //Used to clean up any previous groups the user belongs to on startup
	    public void LeaveOrEndGroup(string memberPhoneNumber)
	    {
	        var member = new GroupMemberVm
	        {
	            PhoneNumber = memberPhoneNumber
	        };
	        var group = FindGroupByGroupId(member);
	        if (!@group.Exists()) return;

	        if(@group.Value.PhoneNumber == memberPhoneNumber) //this user is the group leaderw
	        {
	            EndGroup(memberPhoneNumber);
	        }
	        else
	        {
	            LeaveGroup(@group.Value.PhoneNumber, memberPhoneNumber);
	        }

	    }
        
        public void EndGroup(string groupLeaderPhoneNumber)
        {
            _telemetry.TrackEvent($"EndGroup_Start_LookingFor_{groupLeaderPhoneNumber}");
            var member = new GroupMemberVm
            {
                GroupId = groupLeaderPhoneNumber,
                PhoneNumber = groupLeaderPhoneNumber
            };
            var group = FindGroupByGroupId(member);
            if (!@group.Exists()) return;
            _telemetry.TrackEvent($"EndGroup_GroupFound_{groupLeaderPhoneNumber}");

            Clients.Group(member.GroupId).GroupDisbanded(member.GroupId); //Send Message to group that group is Disbanded
            GroupVm groupVm;
            var removed = StayTogetherGroups.TryRemove(groupLeaderPhoneNumber, out groupVm);
            _telemetry.TrackEvent($"EndGroup_Group_{groupLeaderPhoneNumber}_Ended={removed}");
        }

        public void LeaveGroup(string groupLeaderPhoneNumber, string phoneNumber)
        {
            _telemetry.TrackEvent($"LeaveGroup_Start_{groupLeaderPhoneNumber}_{phoneNumber}_Leaving");
            var member = new GroupMemberVm
            {
                GroupId = groupLeaderPhoneNumber,
                PhoneNumber = phoneNumber
            };
            var group = string.IsNullOrEmpty(member.GroupId) ? FindGroupByMember(member) : FindGroupByGroupId(member);
            if (!@group.Exists()) return;

            var memberAlreadyInGroup = group.Value.GroupMembers.Find(x => x.PhoneNumber == member.PhoneNumber);
            if (memberAlreadyInGroup == null) return;

            Clients.Group(member.GroupId).MemberLeft(memberAlreadyInGroup.PhoneNumber, memberAlreadyInGroup.Name);
            Groups.Remove(Context.ConnectionId, @group.Value.PhoneNumber).ContinueWith(t =>
            {
                @group.Value.GroupMembers.Remove(memberAlreadyInGroup);
                _telemetry.TrackEvent($"LeaveGroup_{groupLeaderPhoneNumber}_{phoneNumber}_Removed");
            });
            
        }

        public void UpdatePosition(GroupMemberVm member)
        {
            member.ConnectionId = Context.ConnectionId;            
            MemberIndex.AddOrUpdate(member.PhoneNumber, member,(key, oldMember) => member);
		    FindGroupAndUpdatePosition(member);
            Clients.All.broadcastUserPosition(member.PhoneNumber.ToBase64(), member.Latitude,
                member.Longitude);
		}

        public List<GroupMemberVm> GetGroupMembers(GroupMemberVm member)
        {
            var group = string.IsNullOrEmpty(member.GroupId) ? FindGroupByMember(member) : FindGroupByGroupId(member);
            return @group.IsValid() ? @group.Value.GroupMembers : new List<GroupMemberVm>();
        }
    
        public void ConfirmGroupInvitation(GroupMemberVm member)
        {
            if(!string.IsNullOrEmpty(member.GroupId))
            {
                var group = FindGroupByGroupId(member);
                var memberAlreadyInGroup = group.Value.GroupMembers.Find(x => x.PhoneNumber == member.PhoneNumber);
                if (memberAlreadyInGroup == null)
                {                    
                    group.Value.GroupMembers.Add(member);
                    memberAlreadyInGroup = member;
                }
                memberAlreadyInGroup.InvitationConfirmed = true;
                Groups.Add(Context.ConnectionId, group.Value.PhoneNumber);//Add the confirmed user to the signalR group
                FindGroupAndUpdatePosition(memberAlreadyInGroup);
                Clients.Group(group.Value.PhoneNumber).requestMemberLocations(group.Value.PhoneNumber);
                _telemetry.TrackEvent("requestMemberLocations_Sent");
            }
        }

	    public void RequestJoinGroup(GroupVm groupVm, string groupLeaderOrMemberPhoneNumber)
	    {
	        _telemetry.TrackEvent("RequestJoinGroup_start");
	        if(!MemberIndex.ContainsKey(groupLeaderOrMemberPhoneNumber)) return;

	        var member= MemberIndex[groupLeaderOrMemberPhoneNumber];
	        var group = string.IsNullOrEmpty(member.GroupId) ? FindGroupByMember(member) : FindGroupByGroupId(member);

	        if(!@group.IsValid()) return;

	        _telemetry.TrackEvent("RequestJoinGroup_sent");

            //This should go to the group leader
	        Clients.Client(member.ConnectionId).joinGroupRequest(groupVm.GroupMembers);
	    }

        public void SendToGroup(GroupMemberVm member, string message)
        {
            if (!string.IsNullOrEmpty(member.GroupId))
            {
                var group = FindGroupByGroupId(member);

                Clients.Group(group.Value.PhoneNumber).groupMessage(member, message);
                _telemetry.TrackEvent("SendToGroup_MessageSent");
            }
        }

        //Currently for testing
        public void Send(string name, string message)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastMessage(name, message);
        }

	    public void InitializeDashboard()
	    {
	        Clients.All.countUpdate(StayTogetherGroups.Count, MemberIndex.Count);

	        Parallel.ForEach(StayTogetherGroups, (group) =>
	        {
	            var groupLeader = group.Value.GroupMembers.FirstOrDefault();
	            if(groupLeader != null)
	            {
	                Clients.All.broadcastGroupPosition(groupLeader.PhoneNumber.ToBase64(), groupLeader.Latitude,
	                    groupLeader.Longitude);
	            }
	        });

	        Parallel.ForEach(MemberIndex, (memberIndex) =>
	        {
	            var member = memberIndex.Value;
	            Clients.All.broadcastUserPosition(member.PhoneNumber.ToBase64(), member.Latitude,
	                member.Longitude);
	        });
	    }

        public void SendErrorMessage(string errorMessage, string memberPhoneNumber)
        {
            // TODO: Log error message using ApplicationInsights
            Trace.WriteLine(string.Format("Error: {0}\nMember: {1}", errorMessage, memberPhoneNumber));
        }

	    public void SendTelemetry(string message, string memberPhoneNumber)
	    {
	        var groupleaderPhone = "[NotInGroup]";
            var member = new GroupMemberVm
            {
                PhoneNumber = memberPhoneNumber
            };
	        var group = FindGroupByMember(member);
	        if(group.IsValid())
	        {
	            groupleaderPhone = group.Value.PhoneNumber;
	        }

	        _telemetry.TrackEvent($"TelemetryFromDevice_{memberPhoneNumber}_{groupleaderPhone}_{message}");
	    }

        #endregion

        #region Private Methods

	    private void RemoveInactiveGroups()
	    {
	        Parallel.ForEach(StayTogetherGroups, (group) =>
	        {
	            if(!@group.Exists() || @group.IsStillAlive()) return;
	            //Send message that group has expired and stop processing
	            Clients.Group(@group.Value.PhoneNumber).groupDisbanded(@group.Value.PhoneNumber);
	            EndGroup(@group.Value.PhoneNumber);
	        });
	    }

	    private void RemoveInactiveMembers()
	    {
	        Parallel.ForEach(MemberIndex, (memberIndex) =>
	        {
	            var member = memberIndex.Value;
	            var timeSinceLastContact = DateTime.Now.ToUniversalTime().Subtract(member.LastContactDateTime.ToUniversalTime());
	            if(timeSinceLastContact > TimeSpan.FromHours(3))
	            {
	                MemberIndex.TryRemove(member.PhoneNumber, out member);
	            }
	        });
	    }

	    private void SendSms(string phoneNumber, string message)
	    {
	        // Your Account SID from twilio.com/console
	       _accountSid = _accountSid ?? ConfigurationManager.AppSettings["TwilioAccountSid"]; //"ACd58bc61977c369dc29028fd2cc1377c4";
	        // Your Auth Token from twilio.com/console
	        _authToken = _authToken ?? ConfigurationManager.AppSettings["TwilioAuthToken"];
	        _fromPhoneNumber = _fromPhoneNumber ?? ConfigurationManager.AppSettings["TwilioFrom"];

	        TwilioClient.Init(
                _accountSid, 
                _authToken);

	        var messageResource = MessageResource.Create(
	            to: new PhoneNumber("+1" + phoneNumber),
	            from: new PhoneNumber(_fromPhoneNumber),
	            body: message);
	       
	    }
	    private async Task SendSmsAsync (string phoneNumber, string message)
	    {
	        // Your Account SID from twilio.com/console
	        _accountSid = _accountSid ?? ConfigurationManager.AppSettings["TwilioAccountSid"]; //"ACd58bc61977c369dc29028fd2cc1377c4";
	        // Your Auth Token from twilio.com/console
	        _authToken = _authToken ?? ConfigurationManager.AppSettings["TwilioAuthToken"];
	        _fromPhoneNumber = _fromPhoneNumber ?? ConfigurationManager.AppSettings["TwilioFrom"];

	        TwilioClient.Init(
	            _accountSid, 
	            _authToken);

	        var messageResource = await MessageResource.CreateAsync(
	            to: new PhoneNumber("+1" + phoneNumber),
	            from: new PhoneNumber(_fromPhoneNumber),
	            body: message);
	       
	    }
        private void SendGroupInviteToNewMembers(GroupVm groupVm, bool force = false)
        {
            _telemetry.TrackEvent("SendGroupInviteToNewMembers_Start"); 
            for (var i = 1; i < groupVm.GroupMembers.Count; i++) //The group leader is member 0
            {
                var member = groupVm.GroupMembers[i];
                var groupWithMember = FindGroupByMember(member);

                if(groupVm.RemoveMembersFromOtherGroups)//This could be wrong.  
                {
                    LeaveOrEndGroup(member.PhoneNumber);                    
                }

                if (groupWithMember.Exists() && !groupVm.RemoveMembersFromOtherGroups)
                {
                    if(groupWithMember.Value.PhoneNumber != groupVm.PhoneNumber)
                    {
                        _telemetry.TrackEvent(
                            $"SendGroupInviteToNewMembers_Member_{member.PhoneNumber}_AlreadyInGroup");
                        //this member is already in a group
                        Clients.Caller.MemberAlreadyInGroup(member.PhoneNumber, member.Name);
                    }
                    else
                    {
                        _telemetry.TrackEvent(
                            $"SendGroupInviteToNewMembers_Member_{member.PhoneNumber}_AlreadyInCurrentGroup");
                    }
                }
                else
                {
                    member.LastContactDateTime = DateTime.Now;

                    if(MemberIndex.ContainsKey(member.PhoneNumber))
                    {
                        var memberInIndex = MemberIndex[member.PhoneNumber];
                        member.ConnectionId = memberInIndex.ConnectionId;
                        _telemetry.TrackEvent($"SendGroupInviteToNewMembers_GroupInvitationTo{member.PhoneNumber}");
                        Clients.Client(member.ConnectionId)
                            .GroupInvitation(groupVm.PhoneNumber, groupVm.GroupMembers[0].Name);
                    }
                    else
                    {
                        var body = $"{groupVm.PhoneNumber} would like you to join a group in Where's Chris.  If you would like to accept, please open or download the app 'Where's Chris' from the store.";
                        SendSms(member.PhoneNumber, body);
                        _telemetry.TrackEvent($"SendGroupInviteToNewMembers_Member_{member.PhoneNumber}_NotFoundInIndex");
                    }
                }
            }
        }

        private void FindGroupAndUpdatePosition(GroupMemberVm member)
        {
	        var group = string.IsNullOrEmpty(member.GroupId) ? FindGroupByMember(member) : FindGroupByGroupId(member);
            var memberInIndex = MemberIndex[member.PhoneNumber];

	        if(group.IsValid()) 
	        {
                //for some weird reason GroupId is null, even though it was set on the client.
                if (string.IsNullOrWhiteSpace(member.GroupId) && member.InvitationConfirmed == false && memberInIndex.InvitationSent == false)
                {
                    Clients.Caller.GroupInvitation(group.Value.PhoneNumber, group.Value.GroupMembers[0].Name);
                    memberInIndex.InvitationSent = true;
                    return;
                }

	            ProcessGroupMemberPosition(member, group);
	        }
	        else
	        {
                if (group.Exists() && !group.IsStillAlive())
                {
                    //Send message that group has expired and stop processing
                    Clients.Group(group.Value.PhoneNumber).groupDisbanded(group.Value.PhoneNumber);
                    EndGroup(group.Value.PhoneNumber);
                }
                else if (!string.IsNullOrWhiteSpace(member.GroupId))
                {
                    Clients.Caller.groupDisbanded(member.GroupId);
                }
	        }

            Clients.All.countUpdate(StayTogetherGroups.Count, MemberIndex.Count);
        }

	    /// <summary>
		/// Calculate distance of group member from the group leader.
		/// </summary>
	    private void ProcessGroupMemberPosition(GroupMemberVm member, KeyValuePair<string, GroupVm> group)
	    {

	        if (ValidateGroup(group) == false) return;

			var groupMember = group.Value.GroupMembers.First(gm => gm.PhoneNumber == member.PhoneNumber);

	        Groups.Add(Context.ConnectionId, group.Value.PhoneNumber); // Add member to SignalR group

	        UpdateCallerWithGroupId(member, group);

			UpdateGroupMemberData(member, group, groupMember);

	        var distanceCalculator = DistanceCalculatorManager.GetDistanceCalculator((int)DistanceCalculationType.CenterOfGravity, Clients);
	        
            //Todo: Implement Center of Gravity and Daisy Chain
            distanceCalculator.CalculateDistance(group, groupMember);

            Clients.Group(group.Key).groupPositionUpdate(group.Value.GroupMembers);

	        Clients.All.broadcastGroupPosition(group.Value.PhoneNumber, group.Value.GroupMembers[0].Latitude,
	            group.Value.GroupMembers[0].Longitude);

	    }

        private void UpdateGroupMemberData(GroupMemberVm member, KeyValuePair<string, GroupVm> @group, GroupMemberVm groupMember)
        {
            @group.Value.LastContactDateTime = DateTime.Now;
            groupMember.ConnectionId = Context.ConnectionId;
            groupMember.LastContactDateTime = DateTime.Now;
            groupMember.Latitude = member.Latitude;
            groupMember.Longitude = member.Longitude;
        }

        private void UpdateCallerWithGroupId(GroupMemberVm member, KeyValuePair<string, GroupVm> @group)
        {
            if (string.IsNullOrEmpty(member.GroupId))
            {
                Clients.Caller.updateGroupId(group.Value.PhoneNumber); //Send the groupId (group leader's phone number) to the client
            }
        }

        private static bool ValidateGroup(KeyValuePair<string, GroupVm> @group)
        {
            if (@group.Value == null)
            {
                Debug.WriteLine("CalculateDistance-group is null");
                return false;
            }

            if (@group.Value.GroupMembers == null)
            {
                Debug.WriteLine("CalculateDistance-group member list is null");
                return false;
            }
            if (@group.Value.GroupMembers.Count < 2)
            {
                Debug.WriteLine(string.Format("CalculateDistance-group member list contains {0} members",
                    @group.Value.GroupMembers.Count));
                return false;
            }
            Debug.WriteLine("CalculateDistance-group seems valid");
            return true;
        }

        private KeyValuePair<string, GroupVm> FindGroupByGroupId(GroupMemberVm member)
        {
            if (!string.IsNullOrWhiteSpace(member.GroupId))
            {
                if (StayTogetherGroups.Count > 0)
                {
                    if (StayTogetherGroups.ContainsKey(member.GroupId))
                    {
                        var group = StayTogetherGroups[member.GroupId];
                        if (group != null && group.GroupMembers != null)
                        {
                            var groupMember = group
                                .GroupMembers.FirstOrDefault(gm => gm.PhoneNumber == member.PhoneNumber);

                            return groupMember != null
                                ? new KeyValuePair<string, GroupVm>(groupMember.PhoneNumber,
                                    StayTogetherGroups[member.GroupId])
                                : new KeyValuePair<string, GroupVm>("", null);
                        }
                    }
                    else
                    {
                        Debug.WriteLine(string.Format("FindGroupByGroupId-Group does not contain key {0}",
                            member.GroupId));
                    }
                }
                else
                {
                    Debug.WriteLine(string.Format("FindGroupByGroupId-Group Count is 0"));
                }
            }
            else
            {
                Debug.WriteLine(string.Format("FindGroupByGroupId-Member.GroupId is empty"));
            }
            return new KeyValuePair<string, GroupVm>("", null);           
        }

        private KeyValuePair<string, GroupVm> FindGroupByMember(GroupMemberVm member)
        {
            foreach (var groupVm in StayTogetherGroups)
            {
                var groupMember = groupVm.Value.GroupMembers.FirstOrDefault(gm => gm.PhoneNumber == member.PhoneNumber);
                if (groupMember != null)
                {
                    return groupVm;
                }
            }
            return new KeyValuePair<string, GroupVm>("", null);
        }
        #endregion

        #region SignalR Events
		public override Task OnConnected()
		{
			// Add your own code here.
			// For example: in a chat application, record the association between
			// the current connection ID and user name, and mark the user as online.
			// After the code in this method completes, the client is informed that
			// the connection is established; for example, in a JavaScript client,
			// the start().done callback is executed.
			return base.OnConnected();
		}

		public override Task OnDisconnected(bool stopCalled)
		{
			// Add your own code here.
			// For example: in a chat application, mark the user as offline, 
			// delete the association between the current connection id and user name.
			return base.OnDisconnected(stopCalled);
		}

		public override Task OnReconnected()
		{
			// Add your own code here.
			// For example: in a chat application, you might have marked the
			// user as offline after a period of inactivity; in that case 
			// mark the user as online again.
			return base.OnReconnected();
		}
        #endregion

	}
}