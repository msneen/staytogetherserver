﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTogetherServer.Extensions
{
    public static class StringExtensions
    {
        public static string ToBase64(this string plainText) {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes).Replace("==", "");
        }
    }
}