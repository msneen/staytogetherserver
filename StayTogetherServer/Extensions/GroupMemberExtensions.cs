﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StayTogetherServer.Models;

namespace StayTogetherServer.Extensions
{
    public static class GroupMemberExtensions
    {
        public static void RemoveDuplicates(this List<GroupMemberVm> groupMemberVms)
        {
            var duplicates = groupMemberVms.GroupBy(s => s.PhoneNumber).SelectMany(grp => grp.Skip(1));
            foreach (var duplicate in duplicates)
            {
                groupMemberVms.Remove(duplicate);
            }
        }

        public static bool AddIfNotIncluded(this List<GroupMemberVm> groupMemberVms, GroupMemberVm member)
        {
            var existingMember = groupMemberVms.FirstOrDefault(m => m.PhoneNumber == member.PhoneNumber);
            if(existingMember != null) return false;

            groupMemberVms.Add(member);
            return true;
        }
    }
}