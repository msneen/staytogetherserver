﻿using System;
using System.Collections.Generic;
using StayTogetherServer.Models;

namespace StayTogetherServer.Extensions
{
    public static class GroupExtensions
    {
        public static bool IsValid(this KeyValuePair<string, GroupVm> group)
        {
            return group.Exists() && group.IsStillAlive();
        }

        public static bool Exists(this KeyValuePair<string, GroupVm> group)
        {
            return group.Value != null;
        }
        

        public static bool IsStillAlive(this KeyValuePair<string, GroupVm> group)
        {
            return group.Value.GroupDisbandDateTime.ToUniversalTime() > DateTime.Now.ToUniversalTime();
        }
    }
}