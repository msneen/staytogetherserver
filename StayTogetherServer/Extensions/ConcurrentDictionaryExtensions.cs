﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StayTogetherServer.Models;

namespace StayTogetherServer.Extensions
{
    public static class ConcurrentDictionaryExtensions
    {
        public static void RemoveGroup(this ConcurrentDictionary<string, GroupVm> stayTogetherGroups, GroupVm groupVm)
        {
            if (stayTogetherGroups.ContainsKey(groupVm.PhoneNumber))
            {
                GroupVm tempGroup;
                stayTogetherGroups.TryRemove(groupVm.PhoneNumber, out tempGroup);
            }
        }
    }
}