﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using StayTogetherServer.Models;

namespace StayTogetherServer.DistanceCalculator
{
    public class CenterOfGravity
    {
        public static Position GetCentralMedianCoordinate(List<GroupMemberVm> groupMembers)
        {
            if (groupMembers == null || groupMembers.Count < 1) throw new Exception("Group must contain GroupMembers");

            var sortedLatitudes = groupMembers.OrderBy(x => x.Latitude).Select(l => l.Latitude).ToList();
            var sortedLongitudes = groupMembers.OrderBy(x => x.Longitude).Select(l => l.Longitude).ToList();

            var midLatitude = sortedLatitudes.Count/2;
            var midLongitude = sortedLongitudes.Count/2;

            var medianLatitude = (sortedLatitudes.Count % 2 != 0)
                ? sortedLatitudes[midLatitude]
                : ((sortedLatitudes[midLatitude] + sortedLatitudes[midLatitude - 1])/2);

            var medianLongitude = (sortedLongitudes.Count % 2 != 0)
                ? sortedLongitudes[midLongitude]
                : ((sortedLongitudes[midLongitude] + sortedLongitudes[midLongitude - 1]) / 2);

            return new Position
            {
                Latitude = medianLatitude,
                Longitude = medianLongitude
            };
        }


        public static Position GetCentralAverageCoordinate(List<GroupMemberVm> groupMembers)
        {
            if (groupMembers.Count == 1)
            {
                var groupMember = groupMembers.Single();
                return new Position
                {
                    Latitude = groupMember.Latitude,
                    Longitude = groupMember.Longitude
                };
            }

            double x = 0;
            double y = 0;
            double z = 0;

            foreach (var geoCoordinate in groupMembers)
            {
                var latitude = geoCoordinate.Latitude * Math.PI / 180;
                var longitude = geoCoordinate.Longitude * Math.PI / 180;

                x += Math.Cos(latitude) * Math.Cos(longitude);
                y += Math.Cos(latitude) * Math.Sin(longitude);
                z += Math.Sin(latitude);
            }

            var total = groupMembers.Count;

            x = x / total;
            y = y / total;
            z = z / total;

            var centralLongitude = Math.Atan2(y, x);
            var centralSquareRoot = Math.Sqrt(x * x + y * y);
            var centralLatitude = Math.Atan2(z, centralSquareRoot);

            return new Position
            {
                Latitude = centralLatitude * 180 / Math.PI,
                Longitude = centralLongitude * 180 / Math.PI
            };
        }

    }
}