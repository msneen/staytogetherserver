﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StayTogetherServer.Models;

namespace StayTogetherServer.DistanceCalculator
{
    public interface IDistanceCalculator
    {
        dynamic Clients { get; set; }
        void CalculateDistance(KeyValuePair<string, GroupVm> group, GroupMemberVm groupMember);
    }
    public class DistanceCalculatorManager
    {
        public static IDistanceCalculator GetDistanceCalculator(int distanceCalculationType, dynamic clients)
        {
            switch (distanceCalculationType)
            {
                case 0:
                    return new DistanceFromLeaderCalculator{Clients = clients};
                case 1:
                    return new DistanceFromCenterOfGravityCalculator { Clients = clients };
                case 2:
                    return new DaisyChainDistanceCalculator { Clients = clients };
                default:
                    return null;
            }
        }
    }
}