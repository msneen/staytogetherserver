﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StayTogetherServer.Models;

namespace StayTogetherServer.DistanceCalculator
{
    public class DistanceFromCenterOfGravityCalculator: IDistanceCalculator
    {
        public dynamic Clients { get; set; }

        public void CalculateDistance(KeyValuePair<string, GroupVm> @group, GroupMemberVm groupMember)
        {
            if (group.Value.GroupMembers.Count < 2) return;

            ProcessByDistanceFromCenterOfGravity(group);
        }

        private void ProcessByDistanceFromCenterOfGravity(KeyValuePair<string, GroupVm> @group)
        {

            var centerPosition = CenterOfGravity.GetCentralMedianCoordinate(group.Value.GroupMembers);
            foreach (GroupMemberVm member in @group.Value.GroupMembers)
            {
                CalculateDistanceFromCenter(member, centerPosition, @group);
            }
        }

        private void CalculateDistanceFromCenter(GroupMemberVm groupMember, Position centerPosition, KeyValuePair<string, GroupVm> @group)
        {
            if (!Distance.LocationsValid(groupMember)) return;

            var distanceFromCenterPosition = Distance.CalculateFeet(centerPosition.Latitude,
                                                                centerPosition.Longitude,
                                                                groupMember.Latitude,
                                                                groupMember.Longitude);

            if (distanceFromCenterPosition > group.Value.MaximumDistance)
            {
                groupMember.LostCount += 1;
                if (groupMember.LostCount > 3)
                {
                    //calculate the geofence and send this if someone gets lost.
                    Clients.Group(group.Key).someoneIsLost(
                        new LostMemberVm
                        {
                            PhoneNumber = groupMember.PhoneNumber,
                            Name = groupMember.Name,
                            Latitude = groupMember.Latitude,
                            Longitude = groupMember.Longitude,
                            LostDistance = distanceFromCenterPosition
                        });
                    groupMember.LostCount = 0;
                }
                else  //count is less than 3.  ask everyone else to send position
                {
                    Clients.Group(group.Key).requestMemberLocations(group.Value.PhoneNumber);
                }
                return;
            }

            groupMember.LostCount = 0;
        }

    }
}