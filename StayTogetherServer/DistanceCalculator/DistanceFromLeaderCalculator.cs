﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using StayTogetherServer.Models;

namespace StayTogetherServer.DistanceCalculator
{
    public class DistanceFromLeaderCalculator:IDistanceCalculator
    {
        public dynamic Clients { get; set; }

        public void CalculateDistance(KeyValuePair<string, GroupVm> @group, GroupMemberVm groupMember)
        {
            ProcessByDistanceFromGroupLeader(group, groupMember);
        }

        //Todo:Make this a strategy of the Strategy Pattern for distance calculation
        private void ProcessByDistanceFromGroupLeader(KeyValuePair<string, GroupVm> @group, GroupMemberVm groupMember)
        {
            var groupLeader = group.Value.GroupMembers[0];

            if (groupLeader.PhoneNumber != groupMember.PhoneNumber)
            {
                CalculateDistanceFromLeaderAndNotifyIfLost(@group, groupLeader, groupMember);
            }
            else
            {
                //This is the group leader, so check against each member
                //Purposely starting with 1, because 0 is the group leader
                for (var i = 1; i < @group.Value.GroupMembers.Count; i++)
                {
                    CalculateDistanceFromLeaderAndNotifyIfLost(@group, groupLeader, @group.Value.GroupMembers[i]);
                }
            }
        }


        private void CalculateDistanceFromLeaderAndNotifyIfLost(KeyValuePair<string, GroupVm> group, GroupMemberVm groupLeader, GroupMemberVm groupMember)
        {
            if (!LocationsValid(groupLeader, groupMember)) return;

            var distanceFromGroupLeader = Distance.CalculateFeet(groupLeader.Latitude,
                                                                groupLeader.Longitude,
                                                                groupMember.Latitude,
                                                                groupMember.Longitude);
            if (distanceFromGroupLeader > 5280*60) return; //Don't report if more than 60 Miles
            if (distanceFromGroupLeader > group.Value.MaximumDistance)           
            {
                //calculate the geofence and send this if someone gets lost.
                Clients.Group(group.Key).someoneIsLost(
                    new LostMemberVm
                    {
                        PhoneNumber = groupMember.PhoneNumber,
                        Name = groupMember.Name,
                        Latitude = groupMember.Latitude,
                        Longitude = groupMember.Longitude,
                        LostDistance = distanceFromGroupLeader
                    });//groupMember.PhoneNumber, groupMember.Latitude, groupMember.Longitude, groupMember.Name, distanceFromGroupLeader
            }

        }

        private static bool LocationsValid(GroupMemberVm groupLeader, GroupMemberVm groupMember)
        {
            if (!Distance.LocationsValid(groupLeader)) return false;
            return Distance.LocationsValid(groupMember);
        }
    }
}