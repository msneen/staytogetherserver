﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StayTogetherServer.Models;

namespace StayTogetherServer.DistanceCalculator
{
    public class DaisyChainDistanceCalculator:IDistanceCalculator
    {
        public dynamic Clients { get; set; }

        public void CalculateDistance(KeyValuePair<string, GroupVm> @group, GroupMemberVm groupMember)
        {
            if (!Distance.LocationsValid(groupMember)) return;

            var isWithinDaisyChain = IsWithinDaisyChain(@group, groupMember);
            if (!isWithinDaisyChain)
            {
                Clients.Group(group.Key).someoneIsLost(new LostMemberVm
                {
                    PhoneNumber = groupMember.PhoneNumber,
                    Name = groupMember.Name,
                    Latitude = groupMember.Latitude,
                    Longitude = groupMember.Longitude,
                    LostDistance = -1.0
                });//groupMember.PhoneNumber, groupMember.Latitude, groupMember.Longitude, groupMember.Name, -1
            }
        }

        private static bool IsWithinDaisyChain(KeyValuePair<string, GroupVm> @group, GroupMemberVm groupMember)
        {
            var isWithinDaisyChain = false;
            foreach (var otherGroupMember in @group.Value.GroupMembers)
            {
                if (groupMember.PhoneNumber == otherGroupMember.PhoneNumber) continue;

                var distanceFromGroupMember = Distance.CalculateFeet(otherGroupMember.Latitude,
                    otherGroupMember.Longitude,
                    groupMember.Latitude,
                    groupMember.Longitude);

                if (distanceFromGroupMember <= @group.Value.MaximumDistance)
                {
                    //if the distance to any other person is within maxDistance, they're good
                    isWithinDaisyChain = true;
                }
            }
            return isWithinDaisyChain;
        }
    }
}