﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTogetherServer.Models
{
	public class GroupVm
	{
		public string PhoneNumber { get; set; }
		public int MaximumDistance { get; set; }
		public DateTime GroupCreatedDateTime { get; set; }
		public DateTime LastContactDateTime { get; set; }
        public int DistanceCalculationType { get; set; }
		/// <summary>
		/// Date supplied by the admin - indicates when to disband the group.
		/// </summary>
		public DateTime GroupDisbandDateTime { get; set; }
		public List<GroupMemberVm> GroupMembers { get; set; }

	    public bool RemoveMembersFromOtherGroups { get; set; }
	}
}