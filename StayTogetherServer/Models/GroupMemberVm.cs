﻿using System;

namespace StayTogetherServer.Models
{
	/// <summary>
	/// A member is assigned to a group when the admin creates the group and passes a list of nicknames and telephone numbers.
	/// </summary>
	public class GroupMemberVm
	{
		public string Name { get; set; }
		public string PhoneNumber { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public bool IsAdmin { get; set; }
		public string GroupId { get; set; }
		public string ConnectionId { get; set; }
		public DateTime LastContactDateTime { get; set; }
        public bool InvitationConfirmed { get; set; }
        public bool InvitationSent { get; set; }
        public int LostCount { get; set; }

        public override string ToString()
        {
            return string.Format("{0}-{1}", Name, PhoneNumber);
        }
	}
}