﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StayTogetherServer.Models
{
	public class ContactSynopsis
	{
		public string Name { get; set; }
		public string PhoneNumber { get; set; }
	}
}